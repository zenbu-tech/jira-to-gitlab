function link_individual_issues {
  # Set issue ID
  ISSUE_ID="${1}"

  # Set variables for accessing the downloaded Jira issue data
  # These folders are also used for storig GitLab data (e.g. JSON output)
  export ISSUE_FOLDER="${PROJECT_WORKING_DIR}/${ISSUE_ID}"

  # Get the IID for the new GitLab issue, you need this for the followup steps
  export GITLAB_ISSUE_IID=$(cat ${ISSUE_FOLDER}/gitlab_issue.json | jq .iid)

  # Get all possible links between issues
  LINKED_EPIC=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}-extended.json | jq -r '.versionedRepresentations.customfield_10009["1"]')
  LINKED_TASKS=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq -r .fields.subtasks[].key)
  LINKED_ISSUES=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq -r .fields.issuelinks[].inwardIssue.key)
  ALL_LINKS="${LINKED_EPIC} ${LINKED_TASKS} ${LINKED_ISSUES}"

  # Create a relates_to link for each linked epic, task, ticket.
  print_log "     ${ISSUE_ID}: Create links for this issue"
  for l in ${ALL_LINKS}
  do
    # Skip to next ${l} if this one is null
    if [[ ${l} == 'null' ]] || [[ ${l} == '' ]]
    then
      continue
    fi

    # Get the issue folder
    T_ISSUE_FOLDER="$(find ${WORKING_DIR} -type d -name "${l}")"

    # Skip if the folder doesnt exist
    if [[ ${T_ISSUE_FOLDER} == '' ]]
    then
      continue
    fi

    # Skip if the gitlab_issue.json doesn't contain an .id
    if ! grep '^{"id"' ${T_ISSUE_FOLDER}/gitlab_issue.json > /dev/null 2>&1
    then
      continue
    fi

    # At this point we know:
    # - The target issue ID isn't empty
    # - The target issue folder exists
    # - The target issue has a GitLab ID
    # It should now be safe to create a link.
    print_log "     ${ISSUE_ID}: Create a link with ${l}"
    # Basically two conditions are possible:
    # 1. {"source_issue":......                  : A successful link.
    # 2. {"message":"Issue(s) already assigned"} : This can happen since we're handeling multiple projects.
    while ! egrep '^{"source_issue"|^{"message":"Issue' ${ISSUE_FOLDER}/gitlab-issue-link-${l}.json > /dev/null 2>&1
    do
      # Get target project id and issue iid
      T_PROJECT_ID=$(cat ${T_ISSUE_FOLDER}/gitlab_issue.json | jq .project_id)
      T_ISSUE_IID=$(cat ${T_ISSUE_FOLDER}/gitlab_issue.json | jq .iid)

      # Create the link
      curl \
        --silent \
        --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${GITLAB_ISSUE_IID}/links \
        --data-urlencode "target_project_id=${T_PROJECT_ID}" \
        --data-urlencode "target_issue_iid=${T_ISSUE_IID}" \
        --data-urlencode "link_type=relates_to" \
        --output ${ISSUE_FOLDER}/gitlab-issue-link-${l}.json
      
      # Wait before trying again
      wait_before_retry
    done
  done
}


function link_issues {
  while IFS=, read -r col1 col2
  do
    # Set the Jira project name: $J_KEY
    set_j_name "${col1}" "${col2}"

    # Set the Jira project key: $J_KEY
    set_j_key "${col1}" "${col2}"

    # Project working dir
    export PROJECT_WORKING_DIR="${WORKING_DIR}/${J_KEY}"

    # Set GitLab project ID
    export GITLAB_PROJECT_ID=$(cat ${PROJECT_WORKING_DIR}/gitlab_project.json | jq .id)

    # Set issue keys: ${ISSUE_KEYS}
    set_issue_keys

    print_log "   ${J_KEY}: Create links for the issues in this project"
    for i in ${ISSUE_KEYS}
    do
      # Rate limiter
      waitforjobs
      
      # Create links for individual issues
      link_individual_issues ${i} &
    done

    # Wait for all background jobs to finish for this project
    wait
  done < ${WORKING_DIR}/jira_projects.csv
}
