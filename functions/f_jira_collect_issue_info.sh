function collect_issue_creation_info {
  # Collect all info needed for creating an issue
  #   ${1}            = Jira issue key
  #   ${ISSUE_FOLDER} = exported var
  # Getting information from these files:
  #   ${ISSUE_FOLDER}/${1}.json
  #   ${ISSUE_FOLDER}/${1}-extended.json
  # This function is bigger then I'd like it to be, but it will become even more unclear if I split it into multiple functions (mostly because of the description magic).


  #
  # Create a fancy title:
  #

  # The result will be: JIRA-ISSUE-KEY: Summary
  export J_TITLE="${1}: $(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.summary | tr -d \")"


  #
  # Get the weight (a.k.a. story points)
  # I ran in to some weird null errors, resulting in this silly if construction...
  #

  # Set a default weight of 0
  export J_WEIGHT="0"

  # Check if the custom field exists
  if cat ${ISSUE_FOLDER}/${1}-extended.json | jq .versionedRepresentations.customfield_10005[] > /dev/null 2>&1
  then
    # Check if the existing custom field is empty
    if [[ "`cat ${ISSUE_FOLDER}/${1}-extended.json | jq .versionedRepresentations.customfield_10005[]`" != "null" ]]
    then
      # Finally, set the WEIGHT based on the custom field containing the story points
      # Jira allows for decimals behind a dot, GitLab only allows (assuming a comma might also be used)
      export J_WEIGHT="$(cat ${ISSUE_FOLDER}/${1}-extended.json | jq .versionedRepresentations.customfield_10005[] | cut -d '.' -f 1 | cut -d ',' -f 1)"
    fi
  fi


  #
  # Get dates
  #

  # Creation
  export J_CREATE_DATE=$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.created)

  # Closed
  export J_CLOSED_DATE=$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.resolutiondate)

  # Last update
  export J_UPDATED_DATE=$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.updated)

  # Use the last updated date if the resolutiondate is null
  # Some issues have the status Done, but don't have a resolution date (which is a seperate action in Jira).
  if [[ ${J_CLOSED_DATE} == 'null' ]] || [[ ${J_CLOSED_DATE} == '' ]]
  then
    export J_CLOSED_DATE="${J_UPDATED_DATE}"
  fi


  #
  # Get all labels
  # Including the things that we will convert to labels
  # This will create regular and scoped labels, you can read about it here: https://docs.gitlab.com/ee/user/project/labels.html
  # All labels will get a prefix, this is important for recognisiing the imported labels.
  #

  # Label prefix, it seems nicer for the end user to use the word jira as prefix (initially I used import/imported).
  export L_PREFIX='jira'

  # Get the Jira ticket type
  # J_ISSUE_TYPE is used in different parts of the script, e.g. to determine of an issue is an epic.
  # J_ISSUE_TYPE_LABEL will be used to create the labels
  export J_ISSUE_TYPE=$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.issuetype.name | tr -d \")
  export J_ISSUE_TYPE_LABEL="${L_PREFIX}::type::${J_ISSUE_TYPE}"

  # Get the original Jira labels
  # Regular label, because there will be many
  export ORIGINAL_LABELS="$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.labels -c | tr -d \[ | tr -d \] | tr -d \")"
  export L_ORIGINAL=""
  if [[ $ORIGINAL_LABELS != '' ]]
  then
    export L_ORIGINAL="$(echo $ORIGINAL_LABELS | tr ',' '\n' | sed "s/^/${L_PREFIX}_/g" | tr '\n' ',' | sed 's/,*$//g')"
  fi

  # Status label
  # Scoped because an issue will only have one status
  export ORIGINAL_STATUS="$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.status.name | tr -d \")"
  export L_STATUS="${L_PREFIX}::status::${ORIGINAL_STATUS}"

  # Priority label
  # Scoped because an issue will only have one priority
  export ORIGINAL_PRIORITY="$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.priority.name | tr -d \")"
  export L_PRIO="${L_PREFIX}::priority::${ORIGINAL_PRIORITY}"

  # Versions label
  # Regular label since an issue can contain multipel versions (at least during my import)
  ORIGINAL_VERSIONS=$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.fixVersions[].name | tr -d \" | tr \\n , | sed 's/,*$//g')
  export L_VERSIONS=""
  if [[ ${ORIGINAL_VERSIONS} != '' ]]
  then
    export L_VERSIONS="$(echo ${ORIGINAL_VERSIONS} | tr ',' '\n' | sed "s/^/${L_PREFIX}_version_/g" | tr '\n' ',' | sed 's/,*$//g')"
  fi

  # Wrapping it up, we will use this variabel to add all labels to the new GitLab issue
  export J_LABELS="${L_ORIGINAL},${L_STATUS},${L_PRIO},${L_VERSIONS},${J_ISSUE_TYPE_LABEL}"


  #
  # Get milestone ID
  #

  # Get sprint info from issue (similar )
  S_NAME=$(cat ${ISSUE_FOLDER}/${1}.json | jq -j .fields.customfield_10007[0].name)
  S_START_DATE=$(cat ${ISSUE_FOLDER}/${1}.json | jq -j .fields.customfield_10007[0].startDate | sed 's/T[0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}.[0-9]\{3\}Z//g')
  S_DUE_DATE=$(cat ${ISSUE_FOLDER}/${1}.json | jq -j .fields.customfield_10007[0].endDate | sed 's/T[0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}.[0-9]\{3\}Z//g')

  # Set milestone filename
  M_TITLE=$(echo "${S_NAME}: ${S_START_DATE} / ${S_DUE_DATE}")
  M_FILENAME=$(echo ${M_TITLE} | tr -dc '[:alnum:]\n\r')

  # If the milestone fle exist
  if stat ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}.json > /dev/null 2>&1
  then
    export M_ID=$(cat ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}.json | jq .id)
  else
    export M_ID=0
  fi

  # Set M_ID to 0 if it's empty (to prevent errors during ticket creation)
  if [[ ${M_ID} == '' ]]; then export M_ID=0; fi


  #
  # Create a description for the new GitLab issue
  # This will contain the original description for the Jira issue, along with extra information which we haven't been able to fit in labels
  #

  # Assigned user
  export J_ASSIGNEE="$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.assignee.displayName | tr -d \") $(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.assignee.emailAddress)"

  # Reporter
  export J_REPORTER="$(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.reporter.displayName | tr -d \") $(cat ${ISSUE_FOLDER}/${1}.json | jq .fields.reporter.emailAddress)"

  # Convert the comment text to markdown
  T_CMD_1="cat ${ISSUE_FOLDER}/${1}.json"
  T_CMD_2="jq .fields.description.content[] -c"
  convert_jira_text "${T_CMD_1}" "${T_CMD_2}"

  # This variable will eventually become the description for the new GitLab issue
  # It will contain the remaining information for the Jira issue
  # Note: Both the issue description and comments are formatted using the same sed/tr magic.
  #       Make sure they stay the same, I still need to make a generic function for this although this works and is only needed in two specific places.
  J_DESCRIPTION="
- **Assignee:** ${J_ASSIGNEE}
- **Reporter:** ${J_REPORTER}
- **Jira URL:** [${J_TITLE}](${JIRA_URL}/browse/${1})

---

${J_TEXT}
"
}
