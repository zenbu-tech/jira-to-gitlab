function set_gitlab_vars {
  # The GitLab URL, without a trailing slash
  # This can be any GitLab server, as long as it supports API version 4.
  export GITLAB_URL='***'

  # The GitLab API token belonging to a user with sufficient permissions on the project your in which you'll be creating the issues.
  # This token should belong to a user with sufficient permissions to create projects under the parent group in GitLab.
  # How to create an API token: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
  export GITLAB_TOKEN='***'

  # Parent GitLab group ID (e.g. JiraImport)
  # All Jira projects will be created under this group, this is done in the following way: JiraProjectGroup --> JiraProject
  # Meaning, each project will have it's own group under this parent group and under each project group is a project for the Jira issues.
  # It's done in this way so that epics and labels stick to these groups/projects instead of poluting other groups and projects.
  export GITLAB_PARENT_GROUP='***'

  # Set the GitLab Group and Project visibility (private, internal, or public)
  # Your choice will depend on the parent group:
  # - Parent group 'private'  : private
  # - Parent group 'internal' : private / internal
  # - Parent group 'public'   : private / internal / public
  # 'private' is the safest choice.
  export GITLAB_VISIBILITY='private'
}
