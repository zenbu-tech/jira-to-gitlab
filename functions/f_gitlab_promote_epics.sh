function promote_individual_epic {
  # Set issue ID
  ISSUE_ID="${1}"

  # Set variables for accessing the downloaded Jira issue data
  # These folders are also used for storig GitLab data (e.g. JSON output)
  export ISSUE_FOLDER="${PROJECT_WORKING_DIR}/${ISSUE_ID}"

  # Get the issue type
  export J_ISSUE_TYPE=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq .fields.issuetype.name | tr -d \")

  # If the issue was an epic, prmote it during these steps
  if [[ ${J_ISSUE_TYPE} == 'Epic' ]]
  then
    # Get the new group ID
    export GITLAB_GROUP_ID=$(cat ${PROJECT_WORKING_DIR}/gitlab_group.json | jq .id)

    # Get the IID for the new GitLab issue, you need this for the followup steps
    export GITLAB_ISSUE_IID=$(cat ${ISSUE_FOLDER}/gitlab_issue.json | jq .iid)

    # Get the issue status
    export J_STATUS="$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq .fields.status.name | tr -d \")"

    # Get issue dates
    export J_CREATE_DATE=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq .fields.created)
    export J_CLOSED_DATE=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq .fields.resolutiondate)
    export J_UPDATED_DATE=$(cat ${ISSUE_FOLDER}/${ISSUE_ID}.json | jq .fields.updated)

    # Use the last updated date if the resolutiondate is null
    # Some issues have the status Done, but don't have a resolution date (which is a seperate action in Jira).
    if [[ ${J_CLOSED_DATE} == 'null' ]] || [[ ${J_CLOSED_DATE} == '' ]]
    then
      export J_CLOSED_DATE="${J_UPDATED_DATE}"
    fi
    
    # Get all links to this issue
    curl \
      --silent \
      --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
      --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/$GITLAB_ISSUE_IID/links \
      --output ${ISSUE_FOLDER}/gitlab_issue_links_all.json
    
    # Store the IID's for these links in a variable
    G_ALL_LINKS_PID=$(cat ${ISSUE_FOLDER}/gitlab_issue_links_all.json | jq -j '.[] | .iid, ",", .project_id,"\n"')

    print_log "     ${ISSUE_ID}: Promote this issue to an epic"

    # A successful epic promotion message contains: {"promote_to_epic":true}
    while ! grep '{"promote_to_epic":true}' ${ISSUE_FOLDER}/gitlab_issue_promoted_epic.json > /dev/null 2>&1
    do
      # Promote the issue to an Epic
      curl \
        --silent \
        --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${GITLAB_ISSUE_IID}/notes \
        --data-urlencode "body=/promote" \
        --data-urlencode "created_at=${J_UPDATED_DATE}" \
        --output ${ISSUE_FOLDER}/gitlab_issue_promoted_epic.json

      # Wait before trying again
      wait_before_retry
    done
    
    # Get all notes so that we can discover the new Epic id.
    curl \
      --silent \
      --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
      --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${GITLAB_ISSUE_IID}/notes \
      --output ${ISSUE_FOLDER}/gitlab_issue_promoted_notes.json
    
    # Get the Epic ID
    export EPIC_ID=$(cat ${ISSUE_FOLDER}/gitlab_issue_promoted_notes.json | jq -j .[0].body | cut -d \& -f 2)

    # Link issues to the new Epic by updating the issue with a quick action
    print_log "     ${ISSUE_ID}: Create links to the new epic ${EPIC_ID}"
    for l in ${G_ALL_LINKS_PID}
    do
      # Skip if empty
      if [[ ${l} == 'null' ]] || [[ ${l} == '' ]]
      then
        continue
      fi

      # Split $a into iid and project_jd
      L_IID=$(echo ${l} | cut -d , -f 1)
      L_PID=$(echo ${l} | cut -d , -f 2)

      print_log "     ${EPIC_ID}: Create a link with issue ${L_IID} in project ${L_PID}"

      # There are two right answers:
      # 1. {"commands_changes":{"epic"
      #    A successful link to an epic.
      # 2. {"commands_changes":{},"summary":["This epic does not exist or you don't have sufficient permission."]}
      #    This is a side effect of importing from Jira. This happens when you try to link an issue from a different project/group to the epic residing in another group (the group hierarchy prevents the link).
      while ! egrep '^\{"commands_changes":\{"epic"|^\{"commands_changes":\{\},"summary":\["This epic' ${ISSUE_FOLDER}/gitlab_issue_promoted_epic_link_${L_IID}.json > /dev/null 2>&1
      do
        # Add a quick action /epic &EPIC_ID
        curl \
          --silent \
          --request POST \
          --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
          --url ${GITLAB_URL}/api/v4/projects/${L_PID}/issues/${L_IID}/notes \
          --data-urlencode "body=/epic &${EPIC_ID}" \
          --data-urlencode "created_at=${J_UPDATED_DATE}" \
          --output ${ISSUE_FOLDER}/gitlab_issue_promoted_epic_link_${L_IID}.json

        # Wait before trying again
        wait_before_retry
      done
    done

    # In case of the status done, close the newly created Epic.
    # A case allows for adding other states lateron.
    case ${J_STATUS} in
      'Done')
        print_log "     ${EPIC_ID}: Close this epic"
        # A successful close starts with an id
        while ! grep '^{"id"' ${ISSUE_FOLDER}/gitlab_issue_promoted_epic_closed.json > /dev/null 2>&1
        do
          # Close epic
          curl \
            --silent \
            --request PUT \
            --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
            --url ${GITLAB_URL}/api/v4/groups/${GITLAB_GROUP_ID}/epics/${EPIC_ID} \
            --data-urlencode "state_event=close" \
            --data-urlencode "updated_at=${J_CLOSED_DATE}" \
            --output ${ISSUE_FOLDER}/gitlab_issue_promoted_epic_closed.json

          # Wait before trying again
          wait_before_retry
        done
        ;;
    esac
  fi
}


function promote_epics {
  while IFS=, read -r col1 col2
  do
    # Set the Jira project name: $J_KEY
    set_j_name "${col1}" "${col2}"

    # Set the Jira project key: $J_KEY
    set_j_key "${col1}" "${col2}"

    # Project working dir
    export PROJECT_WORKING_DIR="${WORKING_DIR}/${J_KEY}"

    # Set GitLab project ID
    export GITLAB_PROJECT_ID=$(cat ${PROJECT_WORKING_DIR}/gitlab_project.json | jq .id)

    # Set issue keys: ${ISSUE_KEYS}
    set_issue_keys

    print_log "   ${J_KEY}: Promote issues from this project to epic"
    for i in ${ISSUE_KEYS}
    do
      # Rate limiter
      waitforjobs

      # Promote individual issue to epic
      promote_individual_epic ${i} &
    done

    # Wait for all background jobs to finish for this project
    wait
  done < ${WORKING_DIR}/jira_projects.csv
}
