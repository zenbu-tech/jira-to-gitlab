function set_script_vars {
  # The folder in which the run script resides
  export RUN_DIR=$(pwd)

  # Set date (using Epoch to set a unique date)
  export START_DATE=$(date +%s)

  # Set working dir, we will store ALL data inside this folder
  # Make sure this volume has enough storage
  # Note: using the date to create a unique folder
  export WORKING_DIR="${HOME}/J2G_${START_DATE}"
  
  # Create the working directory (not a var, but other phases require this directory to exist)
  mkdir -p ${WORKING_DIR}

  # Set the location for the log file
  # The WORKING_DIR is unique, so the log file doesn't need a date
  export LOG_FILE="${WORKING_DIR}/J2G.log"

  # Create the log file (not a var, but other phases will depend on this)
  touch ${LOG_FILE}
}
