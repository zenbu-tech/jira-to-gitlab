function get_sprint_data {
  # Get the sprint data, using ~ as a delimiter
  echo $(cat ${PROJECT_WORKING_DIR}/${1}/${1}.json | jq -j '.fields.customfield_10007[0] | .name + "~" + .state + "~" + .startDate + "~" + .endDate + "~" + .goal' | tr '\n' ' ' | sed 's/T[0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}.[0-9]\{3\}Z//g') >> ${PROJECT_SPRINTS_DIR}/sprints.csv
}


function create_individual_milestone {
  # Input vars:
  # - 1: Name
  # - 2: State
  # - 3: Start date
  # - 4: Due date
  # - 5: Goal

  # Create a unique title (since titles for milestones need to be unique, in Jira not so much...)
  M_TITLE=$(echo "${1}: ${3} / ${4}")

  # Print log
  print_log "     ${J_KEY}: Creating milestone '${M_TITLE}'"

  # Creat a var for the filename
  M_FILENAME=$(echo ${M_TITLE} | tr -dc '[:alnum:]\n\r')

  # A successful create starts with an id
  while ! grep '^{"id"' ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}.json > /dev/null 2>&1
  do
    # Create milestone
    curl \
      --silent \
      --request POST \
      --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
      --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/milestones \
      --data-urlencode "title=${M_TITLE}" \
      --data-urlencode "description=${5}" \
      --data-urlencode "start_date=${3}" \
      --data-urlencode "due_date=${4}" \
      --output ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}.json
    
    # Wait before trying again
    wait_before_retry
  done
  
  # Get milestone ID
  M_ID=$(cat ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}.json | jq .id)

  # Close milestone when state is 'closed'
  if [[ ${2} == 'closed' ]]
  then
    # A successful close starts with an id
    while ! grep '^{"id"' ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}_closed.json > /dev/null 2>&1
    do
      # Close milestone
      curl \
        --silent \
        --request PUT \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/milestones/${M_ID} \
        --data-urlencode "state_event=close" \
        --output ${PROJECT_SPRINTS_DIR}/milestone_${M_FILENAME}_closed.json
      
      # Wait before trying again
      wait_before_retry
    done
  fi
}


function create_all_milestones {
  # Iterate across the CSV file: col1 = key, col2 = name
  while IFS=, read -r col1 col2
  do
    # Set the Jira project name: $J_KEY
    set_j_name "${col1}" "${col2}"

    # Set the Jira project key: $J_KEY
    set_j_key "${col1}" "${col2}"

    # Project working dir
    export PROJECT_WORKING_DIR="${WORKING_DIR}/${J_KEY}"

    # Set variable for sprints dir
    export PROJECT_SPRINTS_DIR="${PROJECT_WORKING_DIR}/SPRINTS"
    
    # Set GitLab project ID
    export GITLAB_PROJECT_ID=$(cat ${PROJECT_WORKING_DIR}/gitlab_project.json | jq .id)

    # Set issue keys
    set_issue_keys

    # Create a folder for storing all information on this projects sprints
    mkdir -p ${PROJECT_SPRINTS_DIR}

    # Create an empty sprints.csv file for this project
    echo -n > ${PROJECT_SPRINTS_DIR}/sprints.csv

    # Get the sprints from all issues in this project
    for i in ${ISSUE_KEYS}
    do
      # Rate limiting
      waitforjobs

      # Get the sprint data
      get_sprint_data ${i} &
    done

    # Wait for all background jobs to finish
    wait

    # Create a unique list of the sprints in this project
    cat ${PROJECT_SPRINTS_DIR}/sprints.csv | sort | uniq > ${PROJECT_SPRINTS_DIR}/sprints_sorted.csv

    # Now we create the milestones at project level
    # - col1: Name
    # - col2: State
    # - col3: Start date
    # - col4: End date
    # - col5: Goal
    while IFS='~' read -r col1 col2 col3 col4 col5
    do
      # Next when the name is empty
      if [[ ${col1} == '' ]]; then continue; fi

      # Rate limiting
      waitforjobs

      # Create milestone
      create_individual_milestone "${col1}" "${col2}" "${col3}" "${col4}" "${col5}" &
    done < ${PROJECT_SPRINTS_DIR}/sprints_sorted.csv

    # Wait for all background jobs to finish
    wait
  done < ${WORKING_DIR}/jira_projects.csv
}
