function set_jira_vars {
  # The Jira cloud URL, without a trailing slash
  # I'm assuming you're connecting to the Jira cloud, thus API version 3.
  # If I'm not mistaken, an on premise installation of Jira only has access to API version 2.
  export JIRA_URL='***'

  # The Jira cloud username, usually an e-mail address.
  # This user needs sufficient permissions in order to download the projects.
  export JIRA_LOGIN='***'

  # The Jira API token belonging to the Jira cloud user.
  # How to create an API token: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
  export JIRA_TOKEN='***'
}
