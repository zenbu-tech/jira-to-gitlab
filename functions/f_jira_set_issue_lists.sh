function set_issue_keys {
  export ISSUE_KEYS=$(cat ${PROJECT_WORKING_DIR}/issue_keys.json | jq .issues[].key | tr -d \")
}


function create_issue_list {
  # J_KEY               = exported var
  # PROJECT_WORKING_DIR = exported var
  # ISSUE_KEYS          = exported var
  # 1                   = active / inactive
  case ${1} in
    'active')
      export ISSUE_LIST=$(for i in ${ISSUE_KEYS}; do cat ${PROJECT_WORKING_DIR}/${i}/${i}.json | jq '. | select(.fields.status.name=="Done" | not)' | jq .key | tr -d \"; done)
    ;;
    'inactive')
      export ISSUE_LIST=$(for i in ${ISSUE_KEYS}; do cat ${PROJECT_WORKING_DIR}/${i}/${i}.json | jq '. | select(.fields.status.name=="Done")' | jq .key | tr -d \"; done)
    ;;
  esac
}
