function convert_jira_text {
  # ${1} = cat command with path to file
  # ${2} = jq command with the fields we're extracting

  # Continue if there is in fact content (not null), this will prevent some errors in the logs.
  if ${1} | ${2} > /dev/null 2>&1
  then
    export J_TEXT=$(${1} | ${2} \
| tr -d '{}[]' \
| sed 's/$/\
/g' \
| sed 's/"type":"inlineCardattrs":"url":/ /' \
| sed 's/"type":"hardBreak"//g' \
| sed 's/"type":"paragraph",//g' \
| sed 's/"content"://g' \
| sed 's/"type":"text",//g' \
| sed 's/"text"://g' \
| sed 's/,"marks":"type":"strong"//g' \
| sed 's/,"marks":"type":"em"//g' \
| sed 's/,"marks":"type":"underline"//g' \
| sed 's/,"marks":"type":"strike"//g' \
| sed 's/,"marks":"type":"link","attrs":"href":/ /g' \
| sed '/blockquote/s/$/,>>>/g' \
| sed '/blockquote/s/,/\
\
/g' \
| sed 's/"type":"blockquote"/\
>>>/g' \
| sed '/codeBlock/s/$/\\n```/g' \
| sed '/codeBlock/s/\\n/\
/g' \
| sed 's/"type":"codeBlock","attrs":,/```\
/g' \
| sed 's/"type":"bulletList",//g' \
| sed 's/"type":"orderedList",//g' \
| sed 's/"type":"listItem",/\
*\ /g' \
| sed '/^"type":"table",.*/s/$/\
```/g' \
| sed 's/"type":"table",/```/g' \
| sed 's/"attrs":"isNumberColumnEnabled":false,//g' \
| sed 's/"layout":"default",//g' \
| sed 's/"type":"tableRow",/\
/g' \
| sed '/tableHeader/s/$/\ |/g' \
| sed 's/"type":"tableHeader","attrs":,/\ |\ /g' \
| sed '/tableCell/s/$/\ |/g' \
| sed 's/"type":"tableCell","attrs":,/\ |\ /g' \
| tr -d '",' \
| sed 's/marks:type:subsupattrs:type:sub//g' \
| sed 's/marks:type:subsupattrs:type:sup//g' \
| sed 's/type:rule/\
---\
/g' \
| sed 's/type:headingattrs:level:1/# /g' \
| sed 's/type:headingattrs:level:2/## /g' \
| sed 's/type:headingattrs:level:3/### /g' \
| sed 's/type:headingattrs:level:4/#### /g' \
| sed 's/type:headingattrs:level:5/##### /g' \
| sed 's/type:headingattrs:level:6/###### /g' \
| sed 's/marks:type:textColorattrs:color:\#[A-Fa-f0-9]\{6\}//g' \
| sed 's/type:mentionattrs:id:[0-9]\{6\}:[A-Fa-f0-9]\{8\}-[A-Fa-f0-9]\{4\}-[A-Fa-f0-9]\{4\}-[A-Fa-f0-9]\{4\}-[A-Fa-f0-9]\{12\}//g' )
  fi
}
