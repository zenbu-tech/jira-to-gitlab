function set_j_name {
  # Jira project name, removing invalid characters
  export J_NAME=$(echo ${2} | tr -dc '[:alnum:]\n\r')
}


function set_j_key {
  # The following case sets the Jira project key
  # I've had one case where the key was in fact the name.
  # Not sure why, but you'll get null pointer errors from jq when this happens (maybe because things were changed inside the Jira project).
  #   `jq: error (at <stdin>:0): Cannot iterate over null (null)`
  # In our case this was a project with ON as key, in that specific case I neded to set it's name as key.
  # You can catch these odd projects with the following case, by either setting their key to be the same as the name or statically assigning they key valuue.
  case ${1} in
  ON)
    export J_KEY=${2}
    ;;
  *)
    export J_KEY=${1}
    ;;
  esac
}
