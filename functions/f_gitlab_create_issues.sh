function create_indivudual_issue {
  # Set issue key
  I_KEY=${1}

  # Set variables for accessing the downloaded Jira issue data
  # These folders are also used for storig GitLab data (e.g. JSON output)
  export ISSUE_FOLDER="${PROJECT_WORKING_DIR}/${I_KEY}"
  export ISSUE_FOLDER_ATTACHMENTS="${ISSUE_FOLDER}/attachments"

  # Collect all information needed for creating an issue in GitLab
  print_log "     ${I_KEY}: Collecting issue information"
  collect_issue_creation_info ${I_KEY}

  # Create the issue in GitLab
  # Docs: https://docs.gitlab.com/ee/api/issues.html#new-issue
  # The title will be truncated to a maximum of 254 characters (255 is the maximum)!
  # The original title wil be shown inside the issue description.
  # Also some extra checks, because of this error during creation:
  #   {"message":"Duplicated issue"}
  # It happens at random, so the job to create the issue will loop until the issue is created.
  # This probably happens due to the vast amount of issues being created at the same time.
  # E.g. 2 issues getting the same GitLab ID, one being created first and the second basically bouncing because the ID is already in use).
  print_log "     ${I_KEY}: Creating issue"
  
  # A successful create starts with an id
  while ! grep '^{"id"' ${ISSUE_FOLDER}/gitlab_issue.json > /dev/null 2>&1
  do
    # Create the issue
    curl \
      --silent \
      --request POST \
      --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
      --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues \
      --data-urlencode "title=$(echo ${J_TITLE} | cut -c 1-254)" \
      --data-urlencode "weight=${J_WEIGHT}" \
      --data-urlencode "created_at=${J_CREATE_DATE}" \
      --data-urlencode "labels=${J_LABELS}" \
      --data-urlencode "description=${J_DESCRIPTION}" \
      --data-urlencode "milestone_id=${M_ID}" \
      --output ${ISSUE_FOLDER}/gitlab_issue.json
    
    # Wait before trying again
    wait_before_retry
  done

  # Get the IID for the new GitLab issue, you need this for the followup steps
  export GITLAB_ISSUE_IID=$(cat ${ISSUE_FOLDER}/gitlab_issue.json | jq .iid)

  # Upload all attachments to the new issue
  # We will create a comment for each attachment
  print_log "     ${I_KEY}: Uploading attachments"
  for a in $(ls ${ISSUE_FOLDER_ATTACHMENTS})
  do
    # Skip if a is empty
    if [[ ${a} == '' ]]
    then
      continue
    fi

    print_log "     ${I_KEY}: ${a}: Uploading to project ID ${GITLAB_PROJECT_ID}"
    # A successful upload starts with an id
    while ! grep '{"alt"' ${ISSUE_FOLDER}/gitlab_issue_attachment_${a}.json > /dev/null 2>&1
    do
      # Upload attachment
      curl \
        --silent \
        --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        --form "file=@${ISSUE_FOLDER_ATTACHMENTS}/${a}" \
        --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/uploads \
        --output ${ISSUE_FOLDER}/gitlab_issue_attachment_${a}.json
      
      # Wait before trying again
      wait_before_retry
    done

    # Create comment body
    ATTACHMENT_COMMENT_BODY="
\`\`\`
$(cat ${ISSUE_FOLDER}/gitlab_issue_attachment_${a}.json | jq)
\`\`\`

---

$(cat ${ISSUE_FOLDER}/gitlab_issue_attachment_${a}.json | jq -j .markdown)
"
    # Add a comment with the attachment to the issue
    # This will use the 'updated' date as creation date (since we don't have the actual attachment upload date to the issue)
    print_log "     ${I_KEY}: ${a}: Adding attachment to issue ID ${GITLAB_ISSUE_IID}"
    # A successful comment starts with an id
    while ! grep '^{"id"' ${ISSUE_FOLDER}/gitlab_issue_attachment_${a}_comment.json > /dev/null 2>&1
    do
      # Add comment
      curl \
        --silent \
        --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${GITLAB_ISSUE_IID}/notes \
        --data-urlencode "body=${ATTACHMENT_COMMENT_BODY}" \
        --data-urlencode "created_at=${J_UPDATED_DATE}" \
        --output ${ISSUE_FOLDER}/gitlab_issue_attachment_${a}_comment.json
      
      # Wait before trying again
      wait_before_retry
    done
  done

  # Add all comments
  print_log "     ${I_KEY}: Adding all comments"
  # Get amount of comments for this ticket
  C_COUNT=$(cat ${ISSUE_FOLDER}/${I_KEY}.json | jq .fields.comment.total)

  # Only run when there are comments
  if [[ ${C_COUNT} -gt 0 ]]
  then
    for (( c=0; c<${C_COUNT}; c++ ))
    do
      # Get created date
      C_CREATED=$(cat ${ISSUE_FOLDER}/${I_KEY}.json | jq .fields.comment.comments[${c}].created)

      # Get author name
      C_AUTHOR_NAME=$(cat ${ISSUE_FOLDER}/${I_KEY}.json | jq .fields.comment.comments[${c}].author.displayName | tr -d \")
      C_AUTHOR_EMAIL=$(cat ${ISSUE_FOLDER}/${I_KEY}.json | jq .fields.comment.comments[${c}].author.emailAddress)

      # Convert the comment text to markdown
      T_CMD_1="cat ${ISSUE_FOLDER}/${I_KEY}.json"
      T_CMD_2="jq .fields.comment.comments[${c}].body.content[] -c"
      convert_jira_text "${T_CMD_1}" "${T_CMD_2}"

      # Set the comment body
      # Note: Both the issue description and comments are formatted using the same sed/tr magic.
      #       Make sure they stay the same, I still need to make a generic function for this although this works and is only needed in two specific places.
      J_COMMENT="
- **Author:** ${C_AUTHOR_NAME} ${C_AUTHOR_EMAIL}

---

${J_TEXT}
"
      
      print_log "     ${I_KEY}: Adding comment ${c}"
      # A successful comment starts with an id
      while ! grep '^{"id"' ${ISSUE_FOLDER}/gitlab_issue_comment_${c}.json > /dev/null 2>&1
      do
        # Add comment to issue
        curl \
          --silent \
          --request POST \
          --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
          --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${GITLAB_ISSUE_IID}/notes \
          --data-urlencode "created_at=${C_CREATED}" \
          --data-urlencode "body=${J_COMMENT}" \
          --output ${ISSUE_FOLDER}/gitlab_issue_comment_${c}.json
        
        # Wait before trying again
        wait_before_retry
      done
    done
  fi

  # Close the issue if the status is Done
  # Not for epics! They need to stay open, we will close those issue after the promotion to epic.
  # Note: I ran into a '/promote' issue when an issue has been closed or reopened.
  print_log "     ${I_KEY}: Close issue if status is Done"
  if [[ "${J_ISSUE_TYPE}" != 'Epic' ]]
  then
    case ${ORIGINAL_STATUS} in
      'Done')
        # A successful close starts with an id
        while ! grep '^{"id"'  ${ISSUE_FOLDER}/gitlab-issue-closed.json > /dev/null 2>&1
        do
          # Close the issue
          curl \
            --silent \
            --request PUT \
            --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
            --url ${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/issues/${GITLAB_ISSUE_IID} \
            --data-urlencode "state_event=close" \
            --data-urlencode "updated_at=${J_CLOSED_DATE}" \
            --output ${ISSUE_FOLDER}/gitlab-issue-closed.json
          
          # Wait before trying again
          wait_before_retry
        done
        ;;
    esac
  fi
}

function create_issues {
  # ${1} = active / inactive
  while IFS=, read -r col1 col2
  do
    # Set the Jira project name: $J_KEY
    set_j_name "${col1}" "${col2}"

    # Set the Jira project key: $J_KEY
    set_j_key "${col1}" "${col2}"

    # Project working dir
    export PROJECT_WORKING_DIR="${WORKING_DIR}/${J_KEY}"

    # Set variable for sprints dir
    export PROJECT_SPRINTS_DIR="${PROJECT_WORKING_DIR}/SPRINTS"

    # Set GitLab project ID
    export GITLAB_PROJECT_ID=$(cat ${PROJECT_WORKING_DIR}/gitlab_project.json | jq .id)

    # Set issue keys
    set_issue_keys

    # Get a list of active issues: $ISSUE_LIST
    create_issue_list ${1}
    
    # Run for each issue
    print_log "   ${J_KEY}: Creating all ${1} issues in GitLab"
    for i in ${ISSUE_LIST}
    do
      # Rate limiter
      waitforjobs
      
      # Create the indivudual issue in a background job
      create_indivudual_issue ${i} &
    done

    # Wait for all background jobs to finish for this project
    wait
  done < ${WORKING_DIR}/jira_projects.csv
}
