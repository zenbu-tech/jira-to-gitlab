function print_log {
  # Generic way of logging to file or screen
  printf "%s\t%s\n" "$(date +"%Y-%m-%d %T")" ": ${1}"
}


function waitforjobs {
  # We need a function to basically rate limit the download and creation of issues
  # In our case, we're downloading and creating +/- 15.000 issue accross 36 projects  
  # Usage of this function, just bevore starting a new background job:
  # waitforjobs
  # job &
  # We do the rate limiting inside this function!
  # The result of this rate limit is that things will be done in batches of ${RATE_LIMIT}.
  RATE_LIMIT='200'
  while test $(jobs -p | wc -w) -ge "${RATE_LIMIT}"
  do
    wait
  done
}


function wait_before_retry {
  # Take a breather
  sleep 1
}
