# Jira to GitLab

A script that will help you import ALL data from the Jira cloud into GitLab (either a private instance or GitLab.com).

This is done using the Jira cloud API (version 3) and the GitLab API (version 4).

## Overview

The script does a lot, I'll try to describe all the steps below:

1. Load all functions
2. Set variables
3. Get all Jira projects
4. Get data from all Jira projects
5. Create all new GitLab groups and projects
6. Create all milestones
7. Create all open/active (status other than 'done') Jira tickets in the new GitLab projects
8. Create all closed/inactive (status 'done') Jira tickets in the new GitLab projects
9. Link all issues based on the information provided by Jira and the newly created issues in GitLab
10. Promote issues to epics and restore all previous linked issues
11. Finished

## Requirements

- GitLab:
  - GitLab instance with supports API version 4 (cloud and on premise use the same API version).
  - GitLab account with write permissions on your target group.
  - Create an API token for this user: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
  - In GitLab, create a new group (or choose an existing group), you will need the ID for this group. The script will create a subgroup and project for each Jira project.
- Jira:
  - Jira cloud with support for API version 3.
    - In our case, we had to move from the cloud to GitLab. In other cases you might want to move from an on premise Jira installation to GitLab. The latter won't work with this script, at least not in its current form. You will then need to convert the Jira calls and jq queries to the Jira json output files to match [Jira API version 2](https://developer.atlassian.com/cloud/jira/platform/rest/v2/intro/). This shouldn't be too hard, although there are subtle difference between these API versions. The script does contain a few references of other scripts that utilize the version 2 API, you might be able to get some direction from them.
  - This script has been tested on the classic Jira projects. I suspect it will also work for the next gen projects, since I'm not doing much with the projects API, but I haven't had the opportunity to verify this.
  - Jira account with read permissions on the projects you wish to migrate.
  - Create an API token for this user: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
- The machine running the script:
  - Make sure you have a stable internet connection.
  - Make sure there's enough storage, since you'll be downloading Jira and all it's contents to this machine.

## Usage

Before running the script, set the variables in these files:

- `./functions/f_vars_script.sh`
- `./functions/f_vars_jira.sh`
- `./functions/f_vars_gitlab.sh`

Once these are set, you can run the script. It's advised to use [tmux](https://github.com/tmux/tmux) or [screen](https://www.gnu.org/software/screen/) to run this script.

```bash
$ screen
$ ./jira2gitlab.sh
# detach the screen: CTRL + A D
```

Once the script is running, you can follow the logfile for the detailed progress:

```bash
# The log file is located inside the ${WORKING_DIR}, check `./functions/f_vars_script.sh` for the exact location.
$ tail -f J2G.log
```

## License

MIT License, see `LICENSE` file for more details.
