#!/bin/bash


#
# Author        : Bart 全部技術
# Version       : 1.0.0
# License       : MIT License
# Usage         : First set all variables in `./functions/f_vars_*.sh`, then run `./jira2gitlab.sh`.
# Description   : Import multiple Jira (API version 3) projects into GitLab (API version 4).
# Benchmark     : I ran an import from the Jira cloud to our local GitLab server (4 cores, 8GB RAM), these were the statistics:
#                 - Jira: 34 projects, 16725 issues, 371 epics, 335 sprints
#                 - GitLab: 34 groups, 34 projects, 16725 issues, 371 epics, 335 milestones (sprints)
#                 - Script: 2,7GB data imported, run time +/- 4 hours.
# Documentation : Below a list of websites that helped me create this script.
#                 - API's:
#                   - Jira API v3: https://developer.atlassian.com/cloud/jira/platform/rest/v3/
#                   - GitLab API v4: https://docs.gitlab.com/ee/api/
#                 - Used tools:
#                   - Bash: https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html
#                   - Homebrew (macOS): https://brew.sh
#                   - curl: https://curl.haxx.se
#                   - jq: https://stedolan.github.io/jq/
#                 - Related GitLab epic:
#                   - https://gitlab.com/groups/gitlab-org/-/epics/2738
#                 - Articles and scripts that gave me inspiration:
#                   - https://about.gitlab.com/blog/2017/08/21/migrating-your-jira-issues-into-gitlab/
#                   - https://medium.com/linagora-engineering/gitlab-rivals-winter-is-here-584eacf1fe9a
#                   - https://gist.github.com/toudi/67d775066334dc024c24
#                   - https://gist.github.com/Gwerlas/980141404bccfa0b0c1d49f580c2d494
#                   - https://gist.github.com/secretrob/b11791f19bc8f72a9ca87943e283b3c4
#


#
# 0. Increase limits, the limits will be set to whatever maximum is allowed on your system (this will prevent some resource errors)
#

# Open files
ulimit -Sn unlimited

# User processes
ulimit -Su unlimited


#
# 1. Load all functions
#

for i in ./functions/f_*.sh ; do source ${i}; done


#
# 2. Set variables
#

set_jira_vars
set_gitlab_vars
set_script_vars


#
# Start logging
#

print_log '1. All functions have been loaded'                          | tee -a ${LOG_FILE}
print_log '2. Variables for Jira, GitLab and the script have been set' | tee -a ${LOG_FILE}
print_log "   Log file: ${LOG_FILE}"                                   | tee -a ${LOG_FILE}


#
# 3. Get all Jira projects
#

print_log '3. Get a list of all Jira projects' | tee -a ${LOG_FILE}
get_jira_projects                              >> ${LOG_FILE} 2>&1


#
# 3.1 Override jira_projects.csv for testing purposes
#

#echo 'KEY,Project Name' > ${WORKING_DIR}/jira_projects.csv


#
# 4. Get data from all Jira projects
#

print_log '4. Get the data from all Jira projects' | tee -a ${LOG_FILE}
get_jira_projects_data                             >> ${LOG_FILE} 2>&1


#
# 5. Create all new GitLab groups and projects
#

print_log '5. Create all new GitLab groups and projects' | tee -a ${LOG_FILE}
create_gitlab_groups_and_projects                        >> ${LOG_FILE} 2>&1


#
# 6. Create all milestones
#

print_log '6. Create all milestones' | tee -a ${LOG_FILE}
create_all_milestones                >> ${LOG_FILE} 2>&1


#
# 7. Create all open/active (status other than 'done') Jira tickets in the new GitLab projects
#

print_log '7. Create all open/active (status other than done) Jira tickets in the new GitLab projects' | tee -a ${LOG_FILE}
create_issues active                                                                                   >> ${LOG_FILE} 2>&1


#
# 8. Create all closed/inactive (status 'done') Jira tickets in the new GitLab projects
#

print_log '8. Create all closed/inactive (status done) Jira tickets in the new GitLab projects' | tee -a ${LOG_FILE}
create_issues inactive                                                                          >> ${LOG_FILE} 2>&1


#
# 9. Link all issues based on the information provided by Jira and the newly created issues in GitLab
#

print_log '9. Link all issues based on the information provided by Jira and the newly created issues in GitLab' | tee -a ${LOG_FILE}
link_issues                                                                                                     >> ${LOG_FILE} 2>&1


#
# 10. Promote issues to epics and restore all previous linked issues
#

print_log '10. Promote issues to epics and restore all previous linked issues' | tee -a ${LOG_FILE}
promote_epics                                                                  >> ${LOG_FILE} 2>&1


#
# 11. Finished
#

print_log '11. Finished'                                                                                                                      | tee -a ${LOG_FILE}
print_log "    All Jira and GitLab logs and data are stored in this folder: ${WORKING_DIR}"                                                   | tee -a ${LOG_FILE}
print_log "    Backup this folder, since it's the raw data gathered from Jira and initial JSON data for everything you've created in GitLab." | tee -a ${LOG_FILE}
statistics                                                                                                                                    | tee -a ${LOG_FILE}
